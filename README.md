# Style transfer using Markov Random Fields

This is an implementation using Keras/theano of
[Combining Markov Random Fields and Convolutional Neural Networks for Image Synthesis](https://arxiv.org/abs/1601.04589).


* `losses_and_patches.py` contains the loss functions
described in the paper
* `models.py` contains the classe implementing
the loss and grad functions
* `MRF_style_transfer.py` are scripts performing the optimization on the
content and style images given in argument

The style of Picasso's self-portrait transferred to a picture of B. Russell
using this implementation:

<img src="http://e-duchesne.gitlab.io/test-results/Bertrand-Russell-small.jpg" height="250">
<img src="http://e-duchesne.gitlab.io/test-results/style.jpg" height="250">
<img src="http://e-duchesne.gitlab.io/test-results/aws-c4-mrf0.jpg" height="250">