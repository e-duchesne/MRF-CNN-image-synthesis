#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: etienne

exemple of use:
python MRF_style_transfer.py content.jpg style.jpg -m vgg19 --content_layers block4_conv2 --style_layers block3_conv1 block4_conv1 --prefix result

"""

import keras.backend as K
import keras.applications as app
import numpy as np
from scipy.ndimage import zoom, imread
from scipy.misc import imsave
import argparse
from scipy.optimize import minimize
from losses_and_patches import preprocessing, postprocessing, gen_bounds
from models import MrfModel

K.set_learning_phase(0)

parser = argparse.ArgumentParser()
parser.add_argument('content_img')
parser.add_argument('style_imgs', nargs='+')
parser.add_argument('--init_img', default=None)
parser.add_argument('--prefix', default='result')
parser.add_argument('--model', '-m', choices=['vgg16', 'vgg19', 'resnet50', 'inceptionV3'], default='vgg19')
parser.add_argument('--content_layers', nargs='+', default=['block4_conv2'])
parser.add_argument('--style_layers', nargs='+', default=['block3_conv1', 'block4_conv1'])
parser.add_argument('--alpha1', '-a1', default=1., type=float)
parser.add_argument('--alpha2', '-a2', default=0.1, type=float)
parser.add_argument('--it', default=200, type=int)
parser.add_argument('--scales', default=5, type=int)

args = parser.parse_args()

if args.model == 'vgg16':
    model = app.vgg16.VGG16(include_top=False)
elif args.model == 'vgg19':
    model = app.vgg19.VGG19(include_top=False)
elif args.model == 'resnet50':
    model = app.resnet50.ResNet50(include_top=False)
else:
    model = app.inception_v3.InceptionV3(include_top=False)

content_layers = args.content_layers
style_layers = args.style_layers

styles = [preprocessing(imread(img)) for img in args.style_imgs]
content_img = imread(args.content_img)
content = preprocessing(content_img)
if args.init_img is not None:
    x0 = preprocessing(imread(args.init_img).astype(K.floatx()))
else:
    x0 = np.random.uniform(0, 255, size=content_img.shape)
    x0 = preprocessing(x0).astype(K.floatx())
iterations = args.it
prefix = args.prefix

# to ensure rescaled input has size greater than 64
scales = min(args.scales, int(np.log2(content.shape[1])-6), int(np.log2(content.shape[2])-6))

config = {'patches_shape': (3, 3), 'alpha1': args.alpha1, 'alpha2': args.alpha2}

# Todo: support for 'channels_first'
assert K.image_data_format() == 'channels_last'
assert K.backend() == 'theano', 'Currently requiring theano (for theano.tensor.nnet.neighbours.images2neibs())'

for i in range(scales, -1, -1):
    print('scaled by 1/{}'.format(2**i))
    styles_resized = [zoom(x, (1, 1/(2**i), 1/(2**i), 1)).astype(K.floatx()) for x in styles]
    content_resized = zoom(content, (1, 1/(2**i), 1/(2**i), 1)).astype(K.floatx())
    shape = content_resized.shape
    bounds = gen_bounds(shape[1:])
    shape_x0 = x0.shape
    x0 = zoom(x0, (1, shape[1]/shape_x0[1], shape[2]/shape_x0[2], 1)).astype(K.floatx())
    mrf = MrfModel(model, content_resized, styles_resized,
                   content_layers=content_layers, style_layers=style_layers,
                   config=config)
    res = minimize(
        lambda x: mrf.loss(x.reshape(shape)),
        x0.flatten(),
        jac=lambda x: mrf.grad(x.reshape(shape)).flatten(),
        method='L-BFGS-B',
        bounds=bounds,
        options={'maxiter': iterations, 'disp': True})
    x0 = res.x.reshape(shape)
    img_res = postprocessing(x0)
    imsave(prefix + str(i) + '.jpg', img_res)
