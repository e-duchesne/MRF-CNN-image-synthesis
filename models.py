#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: etienne
"""

import keras.backend as K
from losses_and_patches import create_patches, total_variation, sq_distance, mrf_loss
import numpy as np


class MrfModel:
    """Model for style transfer using mrf prior"""

    def __init__(self, model, content_img, style_imgs, content_layers=None,
                 style_layers=None,
                 config={'patches_shape': (3, 3), 'alpha1': 1., 'alpha2': 0.001}):

        self.p_shape = config['patches_shape']
        self.alpha1 = config['alpha1']
        self.alpha2 = config['alpha2']

        self.layers = dict([(layer.name, layer) for layer in model.layers])
        self.input = model.input

        # initialize content-related layers
        for layer in content_layers:
            assert layer in self.layers
        self.content_layers = content_layers

        self.content_input = content_img
        self.content_output = {}
        for layer in self.content_layers:
            f_content = K.function([self.input], self.layers[layer].output)
            self.content_output[layer] = f_content([self.content_input])

        # initialize style-related layers
        for layer in style_layers:
            assert layer in self.layers
        self.style_layers = style_layers

        self.style_inputs = style_imgs

        self.patches = {}
        self.preprocess_patches(self.style_layers, self.style_inputs)

        # generate the loss and grad functions for optimization
        self.loss, self.grad = None, None
        self.generate_loss_grad(self.alpha1, self.alpha2)

    def preprocess_patches(self, layers_to_patch, inputs_to_patch):

        for layer in layers_to_patch:
            filters = self.layers[layer].output_shape[3]
            h, w = self.p_shape
            out_patches = create_patches(self.layers[layer].output, (h, w, filters))
            f_patches = K.function([self.input], out_patches)
            self.patches[layer] = np.concatenate(
                [f_patches([x]) for x in inputs_to_patch]
            )

    def generate_loss_grad(self, a1, a2):

        loss = K.variable(0.)

        for layer in self.content_layers:
            loss += sq_distance(self.layers[layer].output, self.content_output[layer])
        loss /= len(self.content_layers)

        if a1 != 0.:
            n = len(self.style_layers)
            for layer in self.style_layers:
                cur_layer = self.layers[layer]
                filters = cur_layer.output_shape[3]
                h, w = self.p_shape
                loss += a1*mrf_loss(cur_layer.output, self.patches[layer], (h, w, filters))/n

        if a2 != 0.:
            loss += a2*total_variation(self.input)

        grad = K.gradients(loss, [self.input])

        self.loss = lambda x: K.function([self.input], loss)([x])

        self.grad = lambda x: K.function([self.input], grad[0])([x])
